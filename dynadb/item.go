package dynadb

// Item is a struct to contain dynamodb items
type Item struct {
	AppName  string `json:"appName"`
	DataURL  string `json:"dataURL"`
	AuthURL  string `json:"authURL"`
	KeyID    string `json:"keyID"`
	SecretID string `json:"secretID"`
}
