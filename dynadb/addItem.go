package dynadb

import (
	"encoding/json"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"

	"fmt"
)

// AddItem adds a new application to the app_data_store table in dynamodb
func AddItem(data []byte) map[string]string {
	// Define struct var for data to be unmarshaled into
	var item Item

	// Define variable for status message
	status := make(map[string]string)

	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))

	// Create DynamoDB client
	svc := dynamodb.New(sess)

	// Unmarshal the json containing the new app info (name, authurl, dataurl, key and secret) into item struct
	err := json.Unmarshal(data, &item)
	if err != nil {
		fmt.Println(err)
		status["error"] = "Unable to create item, missing or malformed data provided"
		return status
	}

	// Marshal the item data to correct map attribute type for dynamodb
	av, err := dynamodbattribute.MarshalMap(item)
	if err != nil {
		status["error"] = "Unable to create item, missing or malformed data provided"
		fmt.Println(err.Error())
		return status
	}

	// Create item in table app_data_store
	tableName := "app_data_store"

	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(tableName),
	}
	_, err = svc.PutItem(input)
	if err != nil {
		status["error"] = "error calling PutItem: " + err.Error()
		return status
	}

	results := ("Successfully added '" + item.AppName + "' to table")
	fmt.Println(results)
	status["success"] = results

	return status
}
