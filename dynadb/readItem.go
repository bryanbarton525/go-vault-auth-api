package dynadb

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

// ReadItem reads item out of dynamodb based on token scope
func ReadItem(data map[string]string) (Item, map[string]string) {
	// Declare variable of type Item (struct)
	item := Item{}
	status := make(map[string]string)
	// initialize new session with aws
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))

	// Create DynamoDB client
	svc := dynamodb.New(sess)
	// Get item from dynamodb
	result, err := svc.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String("app_data_store"),
		Key: map[string]*dynamodb.AttributeValue{
			"appName": {
				S: aws.String(data["appName"]),
			},
		},
	},
	)
	if err != nil {
		status["error"] = "no info found for app listed in token scope."
		return item, status
	}

	// unmarshal results to map
	err = dynamodbattribute.UnmarshalMap(result.Item, &item)
	if err != nil {
		status["error"] = err.Error()
		return item, status
	}
	return item, nil
}
