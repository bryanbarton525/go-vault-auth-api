package main

import (
	"fmt"
	"os"

	"gitlab.com/bryanbarton525/go_vault_auth_api/api"
)

func main() {
	success := initTest()
	if success == false {
		panic("Startup check failed, missing required files or variables.")
	}
	fmt.Println("Rest API v2.0 - Mux Routers")
	api.HandleRequests()
}

// initTest is a function to verify software requirements are defined
func initTest() bool {
	fmt.Println("Checking system config before startup")
	cognitoURL := os.Getenv("cognitoURL")
	jwkURL := os.Getenv("jwkURL")
	fmt.Println("cognitoURL:", cognitoURL)
	fmt.Println("jwkURL:", jwkURL)
	if len(cognitoURL) == 0 {
		return false
	} else if len(jwkURL) == 0 {
		return false
	}
	return true
}
