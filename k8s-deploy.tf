resource "kubernetes_pod" "vault-frontend-api" {
  metadata {
    name = "vault-frontend-api"
    labels = {
      App = "vault_frontend_api"
    }
  }

  spec {
    container {
      image = "214165719400.dkr.ecr.us-east-2.amazonaws.com/go_vault_api:latest"
      name  = "vault-frontend-pod"

      port {
        container_port = 8077
      }
    }
  }
}
resource "kubernetes_service" "vault-frontend-api-service" {
  metadata {
    name = "vault-frontend-pod-service"
  }
  spec {
    selector = {
      App = kubernetes_pod.vault-frontend-api.metadata[0].labels.App
    }
    port {
      port        = 8087
      target_port = 8077
    }

    type = "LoadBalancer"
  }
}
output "lb_ip" {
  value = kubernetes_service.vault-frontend-api-service.load_balancer_ingress[0].ip
}