package vault

// TokenResponse is a struct designed to hold the vault token response
type TokenResponse struct {
	RequestID     string                                  `json:"request_id"`
	LeaseID       string                                  `json:"lease_id"`
	Renewable     bool                                    `json:"renewable"`
	LeaseDuration int                                     `json:"lease_duration"`
	Data          map[string]map[string]map[string]string `json:"data"`
	WrapInfo      string                                  `json:"wrap_info"`
	Warnings      string                                  `json:"warnings"`
	Auth          Auth                                    `json:"auth"`
}

// Auth is a struct for housing the vault token response section called auth
type Auth struct {
	ClientToken   string            `json:"client_token"`
	Accessor      string            `json:"accessor"`
	Policies      []string          `json:"policies"`
	TokenPolicies []string          `json:"token_policies"`
	Metadata      map[string]string `json:"metadata"`
	LeaseDuration int               `json:"lease_duration"`
	Renewable     bool              `json:"renewable"`
	EntityID      string            `json:"entity_id"`
	TokenType     string            `json:"token_type"`
	Orphan        bool              `json:"orphan"`
}
