package vault

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/bryanbarton525/go_vault_auth_api/dynadb"
)

// HitVaultLoginEndpoint function takes vaultdata struct in to hit vault login endpoint for approle and return token
func HitVaultLoginEndpoint(vaultData dynadb.Item) ([]byte, map[string]string) {
	status := make(map[string]string)
	// Create map for json string
	jsondata := make(map[string]string)
	jsondata["role_id"] = vaultData.KeyID
	jsondata["secret_id"] = vaultData.SecretID
	jsonStr, err := json.Marshal(jsondata)
	if err != nil {
		status["error"] = "Unable to retrive app info, missing or malformed data provided from token."
		return nil, status
	}

	req, err := http.NewRequest("POST", vaultData.AuthURL, bytes.NewBuffer(jsonStr))
	if err != nil {
		status["error"] = err.Error()
		return nil, status
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		status["error"] = "Unable to retrive app info, either url on file invalid or system is temporarily unavailable."
		return nil, status
	}
	
	defer resp.Body.Close()
	if resp.Status != "200 OK" {
		status["error"] = resp.Status
		return nil, status
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		status["error"] = err.Error()
		return nil, status
	}
	fmt.Println("response Body:", string(body))
	return body, nil
}

// HitVaultDataEndpoint function uses token to access data from vault and return
func HitVaultDataEndpoint(token string, dataURL string) ([]byte, map[string]string) {
	status := make(map[string]string)
	// Create new request to hit vault with provided info
	req, err := http.NewRequest("GET", dataURL, nil)
	if err != nil {
		status["error"] = err.Error()
	}
	// Add token to header
	req.Header.Add("X-Vault-Token", token)
	// Create new client session
	client := &http.Client{}
	// Submit the request
	resp, err := client.Do(req)
	if err != nil {
		status["error"] = err.Error()
	}
	defer resp.Body.Close()
	if resp.Status != "200 OK" {
		status["error"] = resp.Status
		return nil, status
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		status["error"] = err.Error()
		return nil, status
	}
	return body, nil
}
