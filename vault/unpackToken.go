package vault

import (
	"encoding/json"
)

// UnpackToken function is used to unpack vault token resaponse into a struct
func UnpackToken(response []byte) (TokenResponse, map[string]string) {
	status := make(map[string]string)
	responseStruct := TokenResponse{}
	err := json.Unmarshal(response, &responseStruct)
	if err != nil {
		status["error"] = "unable to unpack token:" + err.Error()
		return responseStruct, status
	}
	return responseStruct, nil
}
