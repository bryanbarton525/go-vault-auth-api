FROM golang:1.14

# Define Arguments
ARG cog_env
ARG jwk_env
ARG AKID
ARG SECRET
ARG REGION

# Run commands and copy files
RUN useradd api
WORKDIR /go/src/go_vault_auth_api
COPY . .
RUN make vault_auth_api

# Switch User
USER api

# Define Env Variables
ENV cognitoURL=cog_env
ENV jwkURL=jwk_env
ENV AWS_ACCESS_KEY_ID=AKID
ENV AWS_SECRET_ACCESS_KEY=SECRET
ENV AWS_SECRET_ACCESS_KEY=SECRET
ENV AWS_REGION=REGION

# Run API
CMD ["go_vault_auth_api"]

