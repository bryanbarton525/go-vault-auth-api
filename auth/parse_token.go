package auth

import (
	"fmt"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

//VerifyToken parses jwt and validates the user provided token with aws cognito
func VerifyToken(token string) (bool, map[string]interface{}) {
	// Path to public key for verification is:
	//https://cognito-idp.{AWS_REGION}/{POOL_ID}}/.well-known/jwks.json <- Store as ENV Variable
	ptoken, err := jwt.Parse(token, getPublicKey)
	if err != nil {
		fmt.Println(err)
		return false, map[string]interface{}{"error": err.Error()}
	}
	claims := ptoken.Claims.(jwt.MapClaims)
	for key, value := range claims {
		fmt.Printf("%s\t%v\n", key, value)
	}
	return true, claims
}

// ParseTokenScope parses decoded token and assigns scope to 3 variables
func ParseTokenScope(token map[string]interface{}) map[string]string {

	parsedScopes := strings.Split(strings.TrimSpace(token["scope"].(string)), "/")

	// Seperate the authentication method from the data scope
	roleData := strings.Split(parsedScopes[1], "_")

	// Assign the individual scopes to their own variables
	app := parsedScopes[0]    // output: test-app
	authMethod := roleData[0] // output: approle
	appData := roleData[1]    // output: testdata

	mScope := map[string]string{"appName": app, "authMethod": authMethod, "appData": appData}
	return mScope
}
