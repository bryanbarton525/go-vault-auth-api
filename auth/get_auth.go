package auth

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/dgrijalva/jwt-go"
	"github.com/lestrrat-go/jwx/jwk"
)

// GetOauthToken is a fuction that is used to call cognito to pull get a jwt based on provided key and secret
// jsonStr is a base64 encoded string of the id and secret seperated by a :
// https://{APP-INTEGRATION-DOMAIN-NAME}}/oauth2/token?grant_type=client_credentials <- store as env variable
func GetOauthToken(userjson []byte) map[string]interface{} {

	jmap := make(map[string]string)
	jsonToken := make(map[string]interface{})
	url := os.Getenv("cognitoURL")
	client := &http.Client{}

	// Return Error response if cognitoURL nil
	if len(url) == 0 {
		// Do some fancy Error Handleing
	}

	err := json.Unmarshal(userjson, &jmap)
	if err != nil {
		fmt.Println(err)
	}

	req, err := http.NewRequest("POST", url, nil)
	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Authorization", "Basic "+jmap["user"])
	resp, err := client.Do(req)
	fmt.Println("resp:", resp)
	jblob, err := ioutil.ReadAll(resp.Body)
	fmt.Println("The BLOB:")
	fmt.Println(string(jblob))
	err = json.Unmarshal(jblob, &jsonToken)

	return jsonToken
}

func getPublicKey(token *jwt.Token) (interface{}, error) {

	// TODO: cache response so we don't have to make a request every time
	// we want to verify a JWT
	jwkURL := os.Getenv("jwkURL")
	set, err := jwk.FetchHTTP(jwkURL)
	if err != nil {
		return nil, err
	}

	keyID, ok := token.Header["kid"].(string)
	if !ok {
		return nil, errors.New("expecting JWT header to have string kid")
	}

	if key := set.LookupKeyID(keyID); len(key) == 1 {
		var raw interface{}
		return raw, key[0].Raw(&raw)
	}

	return nil, fmt.Errorf("unable to find key %q", keyID)
}
