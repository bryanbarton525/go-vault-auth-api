module gitlab.com/bryanbarton525/go_vault_auth_api

go 1.14

require (
	github.com/aws/aws-sdk-go v1.33.5
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.7.4
	github.com/lestrrat-go/jwx v1.0.3
)
