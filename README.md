## Environment Variables to Define
```
# Cognito URL
cognitoURL=https://{APP-INTEGRATION-DOMAIN-NAME}}/oauth2/token?grant_type=client_credentials

# Cognito Public Key URL
jwkURL=https://cognito-idp.{REGION}.amazonaws.com/{COGNTIO_POOL_ID}/.well-known/jwks.json

# Access Key ID
AWS_ACCESS_KEY_ID=AKID

# Secret Access Key
AWS_SECRET_ACCESS_KEY=SECRET

# AWS Region Code
AWS_REGION=us-east-1
```
## Run Docker Container
```
sudo docker run -it -p 3000:8077 -e "cognitoURL=YOUR_COGNITO_URL" -e "jwkURL=YOUR_PUBLIC_KEY_PATH" -e "AWS_SECRET_KEY_ID=YOUR_SECRET_KEY_ID" -e "AWS_SECRET_ACCESS_KEY=YOUR_SECRET_KEY" -e "AWS_REGION=YOUR_REGION" --name vault_auth_api vault_auth_api
```
## API Endpoints
---
#### POST /api/v1/auth/login
API call body must include the provided app key and secret seperated by a `:` and base64 encoded. 

Example json block looks like this:
```
{
"user": "Mjc2dDVrdWpuHW90ZTkyMTRzZm0wdmdvZWo6MW9wa3FmaTBnMTZoOGR0cnY5ZWpnZmF2azRhbmczOJVzYjh2c2Jhbm81OGXjdWphOW1vIg=="
}
```

Example curl command:
```
curl --location --request POST 'localhost/api/v1/auth/login' \
--header 'Content: application/json' \
--header 'Content-Type: application/json' \
--data-raw '{
"user": "Mjc2dDVrdWpuYW9yZTkyMTRzZm0wdmdvZWo6MW9wa3FmaTBnMTZoOGR0cnY5ZWpnZmF2aDRhbmczODVzYjh2cyJhbm81OGdjdWphOW1vNg=="
}'
```
---
#### GET /api/v1/approle/secret
API call must have the token defined in the header of the request with the key of `Token`.

Example cURL command:
```
curl --location --request GET 'localhost/api/v1/approle/secret' \
--header 'Token: eyJraWQiOi62N1E4N2N4STF6b3dkam1McnRyTURjek9OQ05TeUhyNnEwWDlrN3EwekVJPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiIyNzZ0NWt1am5hb3RlOTIxNHNmbTB2Z29laiIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoidGVzdC1hcHBcL2FwcHJvbGVfdGVzdGRhdGEiLCJhdXRoX3RpbWUiOjE1OTMwODU1OTQsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC51cy1lYXN0LTIuYW1hem9uYXdzLmNvbVwvdXMtZWFzdC0yXzIyb2NnVEZiRiIsImV4cCI6MTU5MzA4OTE5NCwiaWF0IjoxNTkzMDg1NTk0LCJ2ZXJzaW9uIjoyLCIqdGkiOiI5ZjNlY2I3OC1hM2ZmLTQyYWQtOTM4MC0yNTcyZDAzYTdiYWUiLCJjbGllbnRfaWQiOiIyNzZ0NWt1am5hb3RlOTIxNHNmbTB2Z29laiJ9.k9NTmQ2x-eM7W4n8d2IPTXBA3BtvuL4L09tETp__KBLIXpDneqinIigNcIC9F5k8vxC7m6Ymy6aLk7PScJhhg5oEYpOxD7s7xFaWc9frAUPna6StuAccv4-yDiaMo32lO_mlwnmfQq3WYRu6Ykj9r4e_7j8_zOBrP58ZCS-F1hALup4a0vdwyyg6Xf0zqNErW0VxLY_d4ZuxOnbf_Um-jeTWW80T_5Aq9aQyDkw83nKHberoI6vouIhN9AgDFa57e4OcBXpZGq9LQuXBY1kbSKdUjc5y7d_GAmnmgd2bFJnzxxAYrR00gW1RlrfpSFHUsA5l7jN9Ljd5zd77nGWnbA'
```
