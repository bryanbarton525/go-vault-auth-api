package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"reflect"

	"gitlab.com/bryanbarton525/go_vault_auth_api/auth"
	"gitlab.com/bryanbarton525/go_vault_auth_api/dynadb"
	"gitlab.com/bryanbarton525/go_vault_auth_api/vault"
	"github.com/gorilla/mux"
)

// HandleRequests serves the api
func HandleRequests() {
	myRouter := mux.NewRouter().StrictSlash(true)
	api := myRouter.PathPrefix("/api/v1").Subrouter()
	api.HandleFunc("/auth/login", getJWT).Methods("POST")
	api.HandleFunc("/dynamodb/add_app", addNewApp).Methods("POST")
	api.HandleFunc("/approle/secret", approleAuth).Methods("GET")
	log.Fatal(http.ListenAndServe(":8077", myRouter))
}

// function called by api endpoint to get cognito jwt token
func getJWT(w http.ResponseWriter, r *http.Request) {
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(reqBody))

	jsonToken := auth.GetOauthToken(reqBody)

	json.NewEncoder(w).Encode(jsonToken)
}

// Function called by api endpoint to add new app to dynamodb
func addNewApp(w http.ResponseWriter, r *http.Request) {

	// Variable to hold token string
	var token string

	// Define map container required scope
	requiredScope := map[string]string{"appName": "dynamodb", "appData": "item", "authMethod": "admin"}

	// Map vairable to hold scopes
	scopes := make(map[string]string)

	// Map variable to collect error message
	status := make(map[string]string)

	// parse header and return its contents or error map
	pheader, headerStatus := ReadHeader(r.Header)
	if headerStatus != nil {
		status["error"] = "Unable to read header"
		json.NewEncoder(w).Encode(&status)
		return
	} else if len(pheader.Token[0]) == 0 {
		status["error"] = "missing or invalid token"
		json.NewEncoder(w).Encode(&status)
		return
	} else {
		token = pheader.Token[0]
	}

	// Verify token is valid and return result and decoded token
	valid, result := auth.VerifyToken(token)
	// If token valid parse and collect scope
	if valid == true {
		scopes = auth.ParseTokenScope(result)
	} else {
		status["error"] = "invalid token or token is expired"
		json.NewEncoder(w).Encode(&status)
		return
	}

	// Verify scopes in jwt meet endpoint requirements
	if reflect.DeepEqual(scopes, requiredScope); false {
		status["error"] = "token error - not authorized"
		json.NewEncoder(w).Encode(&status)
		return
	}

	// Parse Body for new app data
	// body should look like {"appname": "name", "dataurl": "url", "authurl": "url", "keyid": "key", "secretid": "secret"}
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		status["error"] = "missing or invalid body content"
		return
	}
	// add new app to dynamodb items list
	addItemStatus := dynadb.AddItem(reqBody)
	fmt.Println(addItemStatus)
	json.NewEncoder(w).Encode(addItemStatus)

}

func approleAuth(w http.ResponseWriter, r *http.Request) {
	// declare token variable
	var token string
	// Map vairable to hold scopes
	scopes := make(map[string]string)
	// declare response status variable
	var status = make(map[string]string)
	// map variable to hold secretData
	secretResponse := make(map[string]interface{})

	// parse header and return its contents or error map
	pheader, headerStatus := ReadHeader(r.Header)
	if headerStatus != nil {
		status["error"] = "Unable to read header"
		json.NewEncoder(w).Encode(&status)
		return
	} else if len(pheader.Token[0]) == 0 {
		status["error"] = "missing or invalid token"
		json.NewEncoder(w).Encode(&status)
		return
	} else {
		token = pheader.Token[0]
	}
	// Verify token is valid and if so parse the token for scope contents
	valid, result := auth.VerifyToken(token)
	if valid == true {
		scopes = auth.ParseTokenScope(result)
	} else {
		status["error"] = "invalid token or token is expired"
		json.NewEncoder(w).Encode(&status)
		return
	}
	// Get scope define app vault auth info from dynamodb
	appdata, status := dynadb.ReadItem(scopes)
	if status != nil {
		json.NewEncoder(w).Encode(&status)
		return
	}
	// Post creds to vault login endpoint and request a vault token
	vtoken, status := vault.HitVaultLoginEndpoint(appdata)
	if status != nil {
		json.NewEncoder(w).Encode(&status)
		return
	}

	// Unpack the token into struct
	unpackedResponse, status := vault.UnpackToken(vtoken)
	if status != nil {
		json.NewEncoder(w).Encode(&status)
		return
	}

	// Call Vault data endpoint to request the secret
	secretData, status := vault.HitVaultDataEndpoint(unpackedResponse.Auth.ClientToken, appdata.DataURL)
	if status != nil {
		json.NewEncoder(w).Encode(&status)
		return
	}
	fmt.Println(string(secretData))
	err := json.Unmarshal(secretData, &secretResponse)
	if err != nil {
		status["error"] = "Fatal error: Unable to retrieve key/secret. Contact your system administrator."
		json.NewEncoder(w).Encode(&status)
		return
	}

	json.NewEncoder(w).Encode(secretResponse)
}
