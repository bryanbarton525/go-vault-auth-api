package api

import (
	"encoding/json"
)

// MHeader is a stuct used to house header content info
type MHeader struct {
	Token        []string `json:"Token"`
	ContentType  []string `json:"Content-Type"`
	ContentLenth []string `json:"Content-Length"`
}

var messageHeader MHeader

// ReadHeader is a func used to parse the message header and return either map of the error or struct container header info
func ReadHeader(headerContent map[string][]string) (MHeader, map[string]string) {
	// Define error message map
	status := make(map[string]string)

	// Use json marshal and unmarshal to transfer map into struct
	hc, err := json.Marshal(headerContent)
	if err != nil {
		status["error"] = err.Error()
		return messageHeader, status
	}
	err = json.Unmarshal(hc, &messageHeader)
	if err != nil {
		status["error"] = err.Error()
		return messageHeader, status
	}

	return messageHeader, nil
}
